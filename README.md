# Flysystem - Azure Blob Storage

Provides plugin to integrate Azure Blob Storage with Flysystem module.

## Requirements

Requires the Flysystem module and the `league/flysystem-azure-blob-storage` 
library: https://github.com/thephpleague/flysystem-azure-blob-storage

Requires a Azure account and Blob storage setup.

## Install

Install can be done through composer, as dependencies are required:

```bash
composer require drupal/flysystem
composer require drupal/flysystem_az_blob
```

## Setup

TBD

For setup instructions see the Flysystem module README.

## Configuration

You can get all the configuration variables you need from Azure:
- Goto storage account you want to use and then Settings -> Access keys
- Click the show keys button

Make a note of the following:

* DefaultEndpointsProtocol
* AccountName
* AccountKey
* EndpointSuffix
* Container Name (Blob service -> Containers in the storage account)

When you have these, add the below to your `web/sites/default/settings.php` file:


```php
$schemes = [
  'az_blob' => [
    'driver' => 'azure_blob_storage',
    'config' => [
      'name' => '<your account name>',
      'key' => '<your account key>',
      'container' => '<your container name>',
      'prefix' => '<your prefix>',
      'cdn_host_name' => '<you cdn host name>',
      'endpointSuffix' => '<your endpoint suffix>',
      'protocol' => 'https',
    ],
    'cache' => TRUE,
    #'serve_js' => TRUE,
    #'serve_css' => TRUE,
  ],
];
$settings['flysystem'] = $schemes;
```

## Maintainers

TBD
