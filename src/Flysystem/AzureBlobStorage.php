<?php

namespace Drupal\flysystem_az_blob\Flysystem;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\flysystem\Flysystem\Adapter\MissingAdapter;
use Drupal\flysystem\Plugin\FlysystemPluginInterface;
use Drupal\flysystem\Plugin\FlysystemUrlTrait;
use Drupal\flysystem\Plugin\ImageStyleGenerationTrait;
use Drupal\flysystem_az_blob\Flysystem\Adapter\AzureBlobStorageAdapter;
use Exception;
use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drupal plugin for the "Azure Blob Storage" Flysystem adapter.
 *
 * @Adapter(id = "azure_blob_storage")
 */
class AzureBlobStorage implements FlysystemPluginInterface, ContainerFactoryPluginInterface {

  use ImageStyleGenerationTrait;
  use FlysystemUrlTrait {
    getExternalUrl as getDownloadlUrl;
  }

  protected bool $isPublic = TRUE;

  /**
   * Plugin configuration.
   *
   * @var array
   */
  protected array $configuration;

  /**
   * The Client proxy.
   *
   * @var \MicrosoftAzure\Storage\Blob\BlobRestProxy
   */
  protected BlobRestProxy $client;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Constructs an Azure Blob Storage object.
   *
   * @param array $configuration
   *   Plugin configuration array.
   */
  public function __construct(array $configuration) {
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration);
    $instance->logger = $container->get('logger.factory')->get('flysystem_az_blob');
    return $instance;
  }

  /**
   * Gets the Azure Blob Storage client
   *
   * @return BlobRestProxy
   */
  public function getClient(): BlobRestProxy {
    if (!isset($this->client)) {
      $this->client = BlobRestProxy::createBlobService($this->getConnectionString());
    }

    return $this->client;
  }

  /**
   * {@inheritdoc}
   */
  public function getAdapter(): AzureBlobStorageAdapter|MissingAdapter {
    try {
      $adapter = new AzureBlobStorageAdapter($this->getClient(), $this->configuration['container']);
    }
    catch (Exception $e) {
      $this->logger->error($e->getMessage());
      $adapter = new MissingAdapter();
    }
    return $adapter;
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl($uri): string {
    $target = $this->getTarget($uri);

    if (strpos($target, 'styles/') === 0 && !file_exists($uri)) {
      $this->generateImageStyle($target);
    }

    return sprintf('%s/%s', $this->calculateUrlPrefix(), UrlHelper::encodePath($target));
  }

  /**
   * {@inheritdoc}
   */
  public function ensure($force = FALSE): array {
    try {
      $this->getAdapter()->listContents();
    }
    catch (Exception $e) {
      $this->logger->error($e->getMessage());
    }

    return [];
  }

  /**
   * Gets the connection string.
   *
   * @return string
   *   The connection string.
   */
  protected function getConnectionString(): string {
    return "DefaultEndpointsProtocol=" . $this->configuration['protocol'] . ";AccountName=" . $this->configuration['name'] . ";AccountKey=" .
      $this->configuration['key'] . ";EndpointSuffix=" . $this->configuration['endpointSuffix'] . ";";
  }

  /**
   * Calculates the URL prefix.
   *
   * @return string
   *   The URL prefix in the form protocol://[name].blob.[endpointSuffix]/[container].
   */
  protected function calculateUrlPrefix(): string {
    return $this->configuration['protocol'] . '://' . $this->configuration['name'] . '.blob.' .
      $this->configuration['endpointSuffix'] . '/' . $this->configuration['container'];
  }

}
