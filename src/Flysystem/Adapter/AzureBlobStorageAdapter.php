<?php

declare(strict_types = 1);

namespace Drupal\flysystem_az_blob\Flysystem\Adapter;

use League\Flysystem\AzureBlobStorage\AzureBlobStorageAdapter as AzureBlobStorageAdapterBase;

/**
 * Drupal specific overrides.
 */
class AzureBlobStorageAdapter extends AzureBlobStorageAdapterBase {

  /**
   * {@inheritdoc}
   */
  public function getMetadata($path) {
    $metadata = parent::getMetadata($path);

    if ($metadata === FALSE && in_array($path, ['css', 'js'])) {
      return [
        'type' => 'dir',
        'path' => $path,
      ];
    }

    return $metadata;
  }
}
